#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-err58-cpp"

#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Transforms/InstCombine/InstCombine.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Scalar/GVN.h"
#include "KaleidoscopeJIT.hh"
#include <algorithm>
#include <cassert>
#include <cctype>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace kiwi {
    namespace codegen {

        static llvm::LLVMContext TheContext;
        static std::unique_ptr<llvm::orc::KaleidoscopeJIT> TheJIT;
        static llvm::IRBuilder<> Builder(TheContext);
        static std::unique_ptr<llvm::Module> TheModule;
        static std::unique_ptr<llvm::legacy::FunctionPassManager> TheFPM;
        static std::map<std::string, llvm::Value *> NamedValues;

        llvm::Value *log_error(const char *str) {
            fprintf(stderr, "%s", str);
            return nullptr;
        }

        void initialize_module_and_pass_mgr() {
            TheModule = std::make_unique<llvm::Module>("miro-jit", TheContext);
            TheFPM = std::make_unique<llvm::legacy::FunctionPassManager>(TheModule.get());
//            TheFPM->add(llvm::createInstructionCombiningPass());
//            TheFPM->add(llvm::createReassociatePass());
//            TheFPM->add(llvm::createGVNPass());

            TheFPM->doInitialization();
        }
    }

    namespace parser {

        namespace Token {
            enum Token { // negative to prevent collision with ascii
                tok_eof = -1,
                tok_def = -2,
                tok_extern = -3,
                tok_identifier = -4,
                tok_number = -5
            };
        }

        namespace ast {
            class Expr {
            public:
                virtual ~Expr() = default;

                virtual llvm::Value *codegen() = 0;
            };

            class NumExpr : public Expr {
                double num_val_;
            public:
                explicit NumExpr(double val) : num_val_(val) {}

                llvm::Value *codegen() override;
            };

            class VariableExpr : public Expr {
                std::string name_;
            public:
                explicit VariableExpr(std::string name) : name_(std::move(name)) {}

                llvm::Value *codegen() override;
            };

            class BinaryExpr : public Expr {
                char op_;
                std::unique_ptr<Expr> lhs_, rhs_;
            public:
                BinaryExpr(char op, std::unique_ptr<Expr> left, std::unique_ptr<Expr> right)
                        : op_(op), lhs_(std::move(left)), rhs_(std::move(right)) {}

                llvm::Value *codegen() override;
            };

            class CallExpr : public Expr {
                std::string callee_;
                std::vector<std::unique_ptr<Expr>> args_;
            public:
                CallExpr(std::string callee_name, std::vector<std::unique_ptr<Expr>> args)
                        : callee_(std::move(callee_name)), args_(std::move(args)) {}

                llvm::Value *codegen() override;
            };

            using kiwi::codegen::TheContext;
            using kiwi::codegen::NamedValues;

            llvm::Value *NumExpr::codegen() {
                return llvm::ConstantFP::get(TheContext, llvm::APFloat(num_val_));
            }

            llvm::Value *VariableExpr::codegen() {
                // Look this variable up in the function.
                llvm::Value *v = NamedValues[name_];
                if (!v)
                    kiwi::codegen::log_error("Unknown variable name");
                return v;
            }

            llvm::Value *CallExpr::codegen() {
                llvm::Function *callee_f = codegen::TheModule->getFunction(callee_);

                if (!callee_f) return codegen::log_error("Unknown function referenced!");
                if (callee_f->arg_size() != args_.size())
                    return codegen::log_error("Incorrect number of arguments in call");

                typedef std::vector<llvm::Value *> values;
                std::optional<values> args_values = std::invoke([&]() { // generate llvm::Value array of args
                    values args_v;
                    for (unsigned i = 0, e = args_.size(); i != e; ++i) {
                        args_v.push_back(args_[i]->codegen());
                        if (!args_v.back()) return std::optional<values>{};
                    }
                    return std::optional{args_v};
                });
                if (!args_values.has_value()) return nullptr;

                return codegen::Builder.CreateCall(callee_f, *args_values, "call_tmp");
            }

            llvm::Value *BinaryExpr::codegen() {
                llvm::Value *l = lhs_->codegen();
                llvm::Value *r = rhs_->codegen();
                if (!l || !r)
                    return nullptr;

                using codegen::Builder;
                switch (op_) {
                    case '+':
                        return Builder.CreateFAdd(l, r, "add_tmp");
                    case '-':
                        return Builder.CreateFSub(l, r, "sub_tmp");
                    case '*':
                        return Builder.CreateFMul(l, r, "mul_tmp");
                    case '<':
                        l = Builder.CreateFCmpULT(l, r, "cmp_tmp");
                        // Convert bool 0/1 to double 0.0 or 1.0
                        return Builder.CreateUIToFP(l, llvm::Type::getDoubleTy(TheContext),
                                                    "bool_tmp");
                    default:
                        return kiwi::codegen::log_error("invalid binary operator");
                }
            }

            class Prototype {
                std::string name_;
                std::vector<std::string> args_;

            public:
                Prototype(std::string name, std::vector<std::string> args)
                        : name_(std::move(name)), args_(std::move(args)) {}

                const std::string &get_name() const { return name_; }

                llvm::Function *codegen();
            };

            llvm::Function *Prototype::codegen() {
                using llvm::Type;
                std::vector<Type *> args_types(args_.size(), Type::getDoubleTy(TheContext));
                llvm::FunctionType *ft = llvm::FunctionType::get(Type::getDoubleTy(TheContext), args_types, false);
                using llvm::Function;
                llvm::Function *f = Function::Create(ft, Function::ExternalLinkage, name_, codegen::TheModule.get());

                uint16_t i = 0;
                for (auto &arg : f->args()) arg.setName((args_[i++]));

                return f;
            }

            class Function {
                std::unique_ptr<Prototype> proto_;
                std::unique_ptr<Expr> body_;

            public:
                Function(std::unique_ptr<Prototype> proto,
                         std::unique_ptr<Expr> body)
                        : proto_(std::move(proto)), body_(std::move(body)) {}

                llvm::Function *codegen();
            };

            llvm::Function *Function::codegen() {
                llvm::Function *the_function = codegen::TheModule->getFunction((proto_->get_name()));
                if (!the_function) the_function = proto_->codegen();
                if (!the_function) return nullptr;
                if (!the_function->empty())
                    return (llvm::Function *) codegen::log_error("Function cannot be redefined");

                using llvm::BasicBlock;
                BasicBlock *BB = BasicBlock::Create(TheContext, "entry", the_function);
                codegen::Builder.SetInsertPoint(BB);

                NamedValues.clear();
                for (auto &arg : the_function->args()) NamedValues[arg.getName()] = &arg;
                if (llvm::Value *ret_val = body_->codegen()) {
                    codegen::Builder.CreateRet(ret_val);
                    llvm::verifyFunction(*the_function);
                    codegen::TheFPM->run(*the_function);

                    return the_function;
                }
                the_function->eraseFromParent();
                return nullptr;
            }


        }

        static std::string identifier_str; // empty unless tok_identifier
        static double num_val;             // empty unless tok_number

        static int get_tok() {
            static int last_char = ' ';

            auto handle_alpha = [&]() {
                identifier_str = static_cast<char>(last_char);
                while (isalnum(last_char = getchar())) {
                    identifier_str += static_cast<char>(last_char);
                }

                if (identifier_str == "def") return Token::tok_def;
                if (identifier_str == "extern") return Token::tok_extern;
                return Token::tok_identifier;
            };

            auto handle_digit = [&]() {
                std::string num_string;
                do {
                    num_string += static_cast<char>(last_char);
                    last_char = getchar();
                } while (isdigit(last_char) || last_char == '.');
                num_val = strtod(num_string.c_str(), nullptr);
                return Token::tok_number;
            };

            while (isspace(last_char)) last_char = getchar(); //skip whitespace
            if (isalpha(last_char)) return handle_alpha(); //alpha case
            if (isdigit(last_char) || last_char == '.') return handle_digit();  //digit case
            if (last_char == EOF) return Token::tok_eof;

            int this_char = last_char;
            last_char = getchar();
            return this_char;
        }

        static int cur_tok;

        static int get_next_token() {
            return cur_tok = get_tok();
        }

        static std::unique_ptr<ast::Expr> parse_expression();

        static std::unique_ptr<ast::Expr> parse_number_expr() {
            auto result = std::make_unique<ast::NumExpr>(num_val);
            get_next_token(); // consume the number
            return std::move(result);
        }

        static std::unique_ptr<ast::Expr> parse_paren_expr() {
            get_next_token(); // eat (.
            auto v = parse_expression();
            if (!v)
                return nullptr;

            if (cur_tok != ')') {
                fprintf(stderr, "Expected ) to end paren expression!");
                return nullptr;
            }
            get_next_token(); // eat ).
            return v;
        }

        static std::unique_ptr<ast::Expr> parse_identifier_expr() {
            std::string id_name = identifier_str;
            get_next_token();
            if (cur_tok != '(') return std::make_unique<ast::VariableExpr>(id_name);

            get_next_token();
            std::vector<std::unique_ptr<ast::Expr>> args;
            if (cur_tok != ')') {
                for (;;) {
                    if (auto arg = parse_expression()) {
                        args.push_back(std::move(arg));
                    } else {
                        return nullptr;
                    }
                    if (cur_tok == ')') break;
                    if (cur_tok != ',') {
                        fprintf(stderr, "Expected , or ) to close arguments.");
                        return nullptr;
                    }
                    get_next_token();
                }
            }

            get_next_token();
            return std::make_unique<ast::CallExpr>(id_name, std::move(args));
        }

        static std::unique_ptr<ast::Expr> parse_primary() {
            switch (cur_tok) {
                default:
                    assert(false);
                case Token::tok_identifier:
                    return parse_identifier_expr();
                case Token::tok_number:
                    return parse_number_expr();
                case '(':
                    return parse_paren_expr();
            }
        }

        static std::map<char, int> binop_precedence;

        static int get_tok_precedence() {
            if (!isascii(cur_tok))
                return -1;

            // Make sure it's a declared binop.
            int tok_prec = binop_precedence[cur_tok];
            if (tok_prec <= 0) return -1;
            return tok_prec;
        }

        static std::unique_ptr<ast::Expr> parse_binop_rhs(int expr_prec,
                                                          std::unique_ptr<ast::Expr> lhs) {
            // If this is a binop, find its precedence.
            for (;;) {
                int tok_prec = get_tok_precedence();

                // If this is a binop that binds at least as tightly as the current binop,
                // consume it, otherwise we are done.
                if (tok_prec < expr_prec)
                    return lhs;

                int bin_op = cur_tok;
                get_next_token();  // eat binop

                // Parse the primary expression after the binary operator.
                auto rhs = parse_primary();
                if (!rhs)
                    return nullptr;

                int next_prec = get_tok_precedence();
                if (tok_prec < next_prec) {
                    rhs = parse_binop_rhs(tok_prec + 1, std::move(rhs));
                    if (!rhs)
                        return nullptr;
                }

                // Merge LHS/RHS.
                lhs = std::make_unique<ast::BinaryExpr>(bin_op, std::move(lhs),
                                                        std::move(rhs));
            }
        }

        static std::unique_ptr<ast::Expr> parse_expression() {
            auto lhs = parse_primary();
            if (!lhs) return nullptr;
            return parse_binop_rhs(0, std::move(lhs));
        }

        static std::unique_ptr<ast::Prototype> parse_prototype() {
            if (cur_tok != Token::tok_identifier) {
                fprintf(stderr, "no function name in prototype!");
                return nullptr;
            }

            std::string fn_name = identifier_str;
            get_next_token();

            if (cur_tok != '(') {
                fprintf(stderr, "Expected '(' in prototype");
                return nullptr;
            }

            std::vector<std::string> arg_names;
            while (get_next_token() == Token::tok_identifier)
                arg_names.push_back(identifier_str);

            if (cur_tok != ')') {
                fprintf(stderr, "Expected ')' in prototype");
                return nullptr;
            }

            get_next_token(); // eat ')'.
            return std::make_unique<ast::Prototype>(fn_name, std::move(arg_names));
        }

        static std::unique_ptr<ast::Prototype> parse_extern() {
            get_next_token(); // eat extern.
            return parse_prototype();
        }

        static std::unique_ptr<ast::Function> parse_definition() {
            get_next_token(); // eat def.
            auto proto = parse_prototype();
            if (!proto)
                return nullptr;

            if (auto e = parse_expression())
                return std::make_unique<ast::Function>(std::move(proto), std::move(e));
            return nullptr;
        }

        static std::unique_ptr<ast::Function> parse_toplevel_expr() {
            if (auto e = parse_expression()) {
                // Make an anonymous proto.
                auto proto = std::make_unique<ast::Prototype>("__anon_expr",
                                                              std::vector<std::string>());
                return std::make_unique<ast::Function>(std::move(proto), std::move(e));
            }
            return nullptr;
        }

        static void handle_definition() {
            if (auto fn_ast = parse_definition()) {
                if (auto *fn_ir = fn_ast->codegen()) {
                    fn_ir->print(llvm::errs());
                    fprintf(stderr, "Parsed a function definition.\n");
                    fprintf(stderr, "\n");
                }
            } else {
                // Skip token for error recovery.
                get_next_token();
            }
        }

        static void handle_extern() {
            if (auto proto_ast = parse_extern()) {
                if (auto *proto_ir = proto_ast->codegen()) {
                    proto_ir->print(llvm::errs());
                    fprintf(stderr, "Parsed an extern.\n");
                    fprintf(stderr, "\n");
                }
            } else {
                // Skip token for error recovery.
                get_next_token();
            }
        }

        static void handle_toplevel_expression() {
            // Evaluate a top-level expression into an anonymous function.
            if (auto fn_ast = parse_toplevel_expr()) {
                if (auto *fn_ir = fn_ast->codegen()) {
                    auto H = codegen::TheJIT->addModule(std::move(codegen::TheModule));
                    codegen::initialize_module_and_pass_mgr();

                    auto h = codegen::TheJIT->addModule(std::move(codegen::TheModule));
                    codegen::initialize_module_and_pass_mgr();

                    auto expr_symbol = codegen::TheJIT->findSymbol("__anon_expr");
                    assert(expr_symbol && "Function not found");

//                    fprintf(stderr, "Parsed a top-level expr\n");
//                    fn_ir->print(llvm::errs());
//                    fprintf(stderr, "\n");
                    double (*FP)() = (double (*)())(intptr_t)expr_symbol.getAddress().get();
                    fprintf(stderr, "Evaluated to %f\n", FP());

                    // Delete the anonymous expression module from the JIT.
                    codegen::TheJIT->removeModule(H);
                }
            } else {
                // Skip token for error recovery.
                get_next_token();
            }
        }

    }

    static void prime() {
        using parser::get_next_token;
        using parser::binop_precedence;
        binop_precedence['<'] = 10;
        binop_precedence['+'] = 20;
        binop_precedence['-'] = 20;
        binop_precedence['*'] = 40;  // highest.

        fprintf(stderr, "ready> ");
        get_next_token();
    }

    static void main_loop() {
        for (;;) {
            fprintf(stderr, "ready> ");
            switch (parser::cur_tok) {
                case parser::Token::tok_eof:
                    return;
                case ';': // ignore top-level semicolons.
                    parser::get_next_token();
                    break;
                case parser::Token::tok_def:
                    parser::handle_definition();
                    break;
                case parser::Token::tok_extern:
                    parser::handle_extern();
                    break;
                default:
                    parser::handle_toplevel_expression();
                    break;
            }
        }
    }

}

int main() {
    llvm::InitializeNativeTarget();
    llvm::InitializeNativeTargetAsmPrinter();
    llvm::InitializeNativeTargetAsmParser();

    using kiwi::main_loop;
    using kiwi::prime;
    prime();
//    kiwi::codegen::initialize_module_and_pass_mgr();
    kiwi::codegen::TheJIT = std::make_unique<llvm::orc::KaleidoscopeJIT>();

    main_loop();

    return 0;
}

#pragma clang diagnostic pop